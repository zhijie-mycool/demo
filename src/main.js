import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 全局样式导入
import './styles/index.css'
// vant 组件导入
import vant from 'vant'
import 'vant/lib/index.css'
// rem
import 'amfe-flexible'
Vue.config.productionTip = false

Vue.use(vant)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
